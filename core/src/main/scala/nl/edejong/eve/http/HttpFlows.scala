package nl.edejong.eve.http

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model._
import akka.http.scaladsl.settings.ConnectionPoolSettings
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, JsonFraming, Source}
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success, Try}

trait HttpFlows extends PlayJsonSupport {

  implicit class ExtraHttp(httpExt: HttpExt) {

    def singleRequestAs[T](name: String)(uri: Uri)(implicit reads: Reads[T], materializer: ActorMaterializer): Future[T] = {
      implicit val ec: ExecutionContextExecutor = materializer.system.dispatcher
      httpExt.singleRequest(HttpRequest(uri = uri)).flatMap {
        case HttpResponse(StatusCodes.OK, _, entity, _) ⇒
          Unmarshal(entity).to[T]

        case HttpResponse(code, _, entity, _) ⇒
          Unmarshal(entity).to[String].flatMap { str =>
            Future.failed(new IllegalStateException(s"Could not retrieve $name: $code: $str"))
          }
      }
    }

    /**
      * Breaks a multi-page response into multiple requests, all adding to the same source.
      */
    def multipageFlow[T](implicit materializer: ActorMaterializer, system: ActorSystem): Flow[(HttpRequest, T), (Try[HttpResponse], T), NotUsed] = {
      val connectionPoolSettings = ConnectionPoolSettings(system).withPipeliningLimit(1).withMaxConnections(3)
      val superpool = httpExt.superPool[(HttpRequest, Int, T)](settings = connectionPoolSettings)
      Flow[(HttpRequest, T)]
        .map { case (request, t) => (request, (request, 1, t)) } // add request and page number to context object, so we can repeat it
        .via(superpool)
        .flatMapMerge(4, {
          case res@(Success(HttpResponse(StatusCodes.OK, headers, _, _)), (request, 1, t)) ⇒
            // If there is a x-pages header, and the value is more than 1, iterate over the pages
            (for (pages ← headers.find(_ is "x-pages").map(_.value().toInt).filter(_ >= 2)) yield {
              // We have multiple pages, concatenate the original response with a flow of the other pages
              Source.single(res).concat(
                Source.fromIterator(() ⇒ (2 to pages).iterator)
                  .map { page ⇒
                    val newRequest = request.withUri(request.uri.withQuery(request.uri.query().+:("page" → page.toString)))
                    (newRequest, (request, page, t))
                  }
                  .via(superpool))
            }).getOrElse(Source.single(res)) // Otherwise, return only the single res object

          case res@(Success(HttpResponse(StatusCodes.OK, headers, _, _)), (request, n, t)) ⇒
            println(s"Got page $n")
            Source.single(res)

          case (Success(HttpResponse(statusCode, headers, entity, _)), (request, n, t)) ⇒
            entity.discardBytes(materializer)
            throw new IllegalStateException(s"Unexpected statusCode for request of page $n of $t")

          case (Failure(e), ctx) ⇒ throw e
        })
        .map { case (response, (_, _, t)) ⇒ (response, t) } // remove request from context object
    }

    def httpIdFlow[Id, T](name: String)(idF: Id ⇒ Uri)(implicit reads: Reads[T], materializer: ActorMaterializer, system: ActorSystem): Flow[Id, (T, Id), NotUsed] =
      Flow[Id].map(id ⇒ (HttpRequest(uri = idF(id)), id))
        .via(httpExt.multipageFlow[Id])
        .flatMapConcat {

          case (Success(HttpResponse(StatusCodes.OK, _, entity, _)), id) ⇒
            entity.dataBytes
              .via(JsonFraming.objectScanner(2 << 16))
              .map(bs ⇒ Json.parse(bs.toArray).validate[T] match {
                case JsSuccess(t, _) ⇒ (t, id)
                case JsError(error) ⇒
                  entity.discardBytes(materializer)
                  throw new IllegalStateException(s"Could not parse object ${bs.toString} in response: $error")
              })

          case (Success(HttpResponse(statusCode, _, entity, _)), id) ⇒
            entity.discardBytes(materializer)
            throw new IllegalStateException(s"Could not retrieve $name for id $id. Statuscode: $statusCode")

          case (Failure(e), id) ⇒ throw new IllegalStateException(s"Could not retrieve $name for id $id: ${e.getMessage}")
        }
  }

}
