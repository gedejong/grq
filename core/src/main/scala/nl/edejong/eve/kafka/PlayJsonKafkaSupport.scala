package nl.edejong.eve.kafka

import java.util

import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.{Deserializer, Serializer}
import play.api.libs.json.{JsSuccess, Json, Reads, Writes}

trait PlayJsonKafkaSupport {

  class PlayJsonSerializer[T](implicit writes: Writes[T]) extends Serializer[T] {
    override def configure(configs: java.util.Map[String, _], isKey: Boolean): Unit = {}

    override def serialize(topic: String, data: T): Array[Byte] =
      if (data == null) null else Json.toBytes(writes.writes(data))

    override def close(): Unit = {}
  }

  class PlayJsonDeserializer[T >: Null](implicit reads: Reads[T]) extends Deserializer[T] {
    override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

    override def close(): Unit = {}

    override def deserialize(topic: String, data: Array[Byte]): T =
      if (data == null) null
      else
        Json.parse(data).validate[T] match {
          case JsSuccess(value, _) => value
          case e => throw new SerializationException(s"Could not deserialize: $e")
        }
  }

}