package nl.edejong.eve

import akka.actor.{Actor, ActorLogging, Props}
import nl.edejong.eve.actors.EveActors

import scala.concurrent.ExecutionContextExecutor

trait DiagnosticActors extends EveActors {

  trait DiagnosticMarketActors extends MarketActors {

    object ToLogActor {
      def props() = Props(new ToLogActor())
    }

    class ToLogActor extends Actor with ActorLogging {

      import MarketActorBehaviour._

      override def receive: Receive = {
        case PriceNotification(regionId, typeId, order, historicPrice) ⇒
          implicit val executionContext: ExecutionContextExecutor = context.dispatcher
          for {
            currentRegion ← region(regionId)
            currentType ← eveType(typeId)
          } {
            val percentage = (1 - (order.price / historicPrice)) * 100
            log.info(
              "%s is on offer in region: %s for a price of %.0f which is %.2f%% below the average weekly price of %f. Order: %s".format(
                currentType.name, currentRegion.name, order.price, percentage, historicPrice, order))
          }
      }
    }
  }
}
