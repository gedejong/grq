package nl.edejong.eve.actors

import java.time.LocalDate

import akka.Done
import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, Props}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import nl.edejong.eve.api.EveAPIs
import nl.edejong.eve.api.domain.Domain

trait EveActors extends Domain with EveAPIs {

  trait MarketActors extends MarketAPI with UniverseAPI {

    object MarketActorBehaviour {

      sealed trait MarketActorMessage

      case class PriceNotification(regionId: RegionId, typeId: TypeId, order: Order, historicPrice: Double) extends MarketActorMessage

    }

    trait RegionTypePriceStreams {

      import concurrent.duration._

      private[this] def calcAverage(prices: Seq[HistoricOrder]): Double = {
        val (sumPrice, sumVolume) = prices
          .filter(order ⇒ order.date.isAfter(LocalDate.now().minusWeeks(1)))
          .foldLeft((0D, 0L)) {
            case ((curSumPrice, curSumVolume), historicOrder) ⇒ (curSumPrice + historicOrder.average, curSumVolume + historicOrder.volume)
          }
        sumPrice / sumVolume
      }

      def averageRegionTypePriceSource(regionId: RegionId, typeId: TypeId)(implicit materializer: ActorMaterializer): Source[Double, Cancellable] =
        Source.tick[Any](0.seconds, 1.day, AnyRef)
          .mapAsync(5)(_ ⇒ historicOrders(regionId, typeId))
          .map(calcAverage)
    }

    object RegionTypePriceActor {
      def props(listener: ActorRef, regionId: RegionId, typeId: TypeId) = Props(new RegionTypePriceActor(listener: ActorRef, regionId, typeId))
    }

    class RegionTypePriceActor(listener: ActorRef, regionId: RegionId, typeId: TypeId) extends Actor
      with RegionTypePriceStreams
      with ActorLogging {

      import MarketActorBehaviour._

      implicit val materializer: ActorMaterializer = ActorMaterializer()
      val priceStream: Cancellable = averageRegionTypePriceSource(regionId, typeId).to(Sink.actorRef(self, Done)).run()

      override def receive: Receive = waitingForFirstPrice()

      override def postStop(): Unit = {
        super.postStop()
        priceStream.cancel()
      }

      def waitingForFirstPrice(waitingOrders: List[Order] = Nil): Receive = {
        case d: Double ⇒
          log.debug(s"New price for region $regionId, $typeId: $d")
          context.become(priceSet(d))
          for (o ← waitingOrders) self ! o

        case o: Order ⇒ context.become(waitingForFirstPrice(o :: waitingOrders))
      }

      def priceSet(historicPrice: Double, notifiedOrderIds: Set[Long] = Set()): Receive = {
        case newPrice: Double ⇒
          log.debug(s"New price for region $regionId, $typeId: $historicPrice")
          context.become(priceSet(newPrice, notifiedOrderIds))

        case order: Order if !order.isBuyOrder && order.price < historicPrice * (1 - .3) && !notifiedOrderIds.contains(order.orderId) ⇒
          listener ! PriceNotification(regionId, typeId, order, historicPrice)
          context.become(priceSet(historicPrice, notifiedOrderIds + order.orderId))
      }
    }

    object GlobalOrdersActor {
      def props(listener: ActorRef) = Props(new GlobalOrdersActor(listener))
    }

    class GlobalOrdersActor(listener: ActorRef) extends Actor {
      override def receive: Receive = managingActorRefs()

      private type ManagedKey = (RegionId, TypeId)

      def managingActorRefs(actorRefs: Map[ManagedKey, ActorRef] = Map()): Receive = {
        case (o: Order, regionId: RegionId) if actorRefs.contains((regionId, o.typeId)) ⇒
          actorRefs((regionId, o.typeId)) ! o

        case (o: Order, regionId: RegionId) ⇒
          val newActorRef = context.actorOf(RegionTypePriceActor.props(listener, regionId, o.typeId))
          newActorRef ! o
          context become managingActorRefs(actorRefs + ((regionId, o.typeId) → newActorRef))

        case unexpected ⇒
          println(s"Unexpected message: $unexpected")
      }
    }

  }

}

