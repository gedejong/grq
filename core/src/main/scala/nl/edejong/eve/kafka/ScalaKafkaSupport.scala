package nl.edejong.eve.kafka

import java.util

import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.{Deserializer, Serializer}

trait ScalaKafkaSupport {

  class LongSerializer extends Serializer[Long] {
    override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

    override def serialize(topic: String, data: Long): Array[Byte] = {
      Array[Byte](
        (data >>> 56).byteValue(),
        (data >>> 48).byteValue(),
        (data >>> 40).byteValue(),
        (data >>> 32).byteValue(),
        (data >>> 24).byteValue(),
        (data >>> 16).byteValue(),
        (data >>> 8).byteValue(),
        data.byteValue())
    }

    override def close(): Unit = {}
  }

  class LongDeserializer extends Deserializer[Long] {
    override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

    override def close(): Unit = {}

    override def deserialize(topic: String, data: Array[Byte]): Long =
      if (data == null) throw new SerializationException("Cannot serialize null data in LongDeserializer")
      else if (data.length != 8) throw new SerializationException("Data size of long is not 8")
      else {
        var value: Long = 0L
        var i: Int = 0
        while (i < 8) {
          value <<= 8
          value |= (data(i) & 255).toLong
          i += 1
        }
        value
      }
  }

}
