package nl.edejong.eve.api

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import com.typesafe.config.Config
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import nl.edejong.eve.api.formats.DomainFormats
import nl.edejong.eve.http.HttpFlows

import scala.collection.immutable
import scala.concurrent.{ExecutionContextExecutor, Future}

trait EveHttpAPIConfiguration {
  def baseUri: Uri
}

trait HoconConfiguration {
  def config: Config
}

trait HoconEveHttpAPIConfiguration extends EveHttpAPIConfiguration with HoconConfiguration {
  val eveConfig: Config = config.getConfig("eve")
  val baseUri: Uri = Uri(eveConfig.getString("base-uri"))
}

trait EveHttpAPIs extends EveAPIs with DomainFormats with PlayJsonSupport with EveHttpAPIConfiguration {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  trait HttpMarketAPI extends MarketAPI with Markets with MarketFormats with HttpFlows {
    private[this] val marketsURI = Uri("./markets/").resolvedAgainst(baseUri)

    override def orders(regionId: RegionId)(implicit materializer: ActorMaterializer): Future[immutable.Seq[Order]] =
      Source.single(regionId).via(ordersFlow).map(_._1).toMat(Sink.seq)(Keep.right).run()

    // Input: region ids, output: orders and region id
    override val ordersFlow: Flow[RegionId, (Order, RegionId), NotUsed] =
      Http().httpIdFlow[RegionId, Order]("orders") { id ⇒
        Uri("%d/orders/?order_type=all".format(id)).resolvedAgainst(marketsURI)
      }

    override def historicOrders(regionId: RegionId, typeId: TypeId)(implicit materializer: ActorMaterializer): Future[immutable.Seq[HistoricOrder]] =
      Source.single((regionId, typeId)).via(historicOrdersFlow).map(_._1).toMat(Sink.seq)(Keep.right).run()

    // Input: region and type, output historic order, region and type
    override val historicOrdersFlow: Flow[(RegionId, TypeId), (HistoricOrder, (RegionId, TypeId)), NotUsed] =
      Http().httpIdFlow[(RegionId, TypeId), HistoricOrder]("historic orders") {
        case (regId, typeId) => Uri("%d/history/?type_id=%d".format(regId, typeId)).resolvedAgainst(marketsURI)
      }

    override def types(regionId: Long)(implicit materializer: ActorMaterializer): Future[Seq[TypeId]] =
      Http().singleRequestAs[Seq[TypeId]](s"types in region $regionId")(Uri("%d/types/".format(regionId)).resolvedAgainst(marketsURI))
  }

  trait HttpUniverseAPI extends UniverseAPI with Universe with UniverseFormats with HttpFlows {
    private[this] val regionsURI = Uri("./universe/regions/").resolvedAgainst(baseUri)
    private[this] val typesURI = Uri("./universe/types/").resolvedAgainst(baseUri)

    override def regionIds: Future[Seq[RegionId]] = Http().singleRequestAs[Seq[Long]]("regions")(regionsURI)

    override def region(id: RegionId): Future[Region] =
      Http().singleRequestAs[Region](s"region $id")(Uri(id.toString + "/").resolvedAgainst(regionsURI))

    override def typeIds: Future[Seq[TypeId]] = Http().singleRequestAs[Seq[Long]]("types")(typesURI)

    override def eveType(id: RegionId): Future[EveType] =
      Http().singleRequestAs[EveType](s"type $id")(Uri(id.toString + "/").resolvedAgainst(typesURI))
  }

}
