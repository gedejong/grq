package nl.edejong.eve.kafka

import com.typesafe.config.Config

trait HoconEveKafkaConfiguration extends EveKafkaConfiguration {
  protected def config: Config

  protected lazy val kafkaConfiguration: Config = config.getConfig("eve-kafka")
  override lazy val bootstrapServers: String = kafkaConfiguration.getString("bootstrap-servers")

  trait HoconOrderKafkaStreamConfiguration extends OrderKafkaStreamConfiguration {
    protected lazy val ordersConfiguration: Config = kafkaConfiguration.getConfig("orders")
    override lazy val topicId: String = ordersConfiguration.getString("topic-id")
  }

  trait HoconOrderKafkaSourceConfiguration extends OrderKafkaSourceConfiguration with HoconOrderKafkaStreamConfiguration {
    protected lazy val sourceConfiguration: Config = ordersConfiguration.getConfig("source")
    override lazy val groupId: String = sourceConfiguration.getString("group-id")
  }

  trait HoconOrderKafkaSinkConfiguration extends OrderKafkaSinkConfiguration with HoconOrderKafkaStreamConfiguration

}
