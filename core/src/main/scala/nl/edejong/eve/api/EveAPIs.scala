package nl.edejong.eve.api

import akka.NotUsed
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import nl.edejong.eve.api.domain.Domain
import nl.edejong.eve.api.formats.DomainFormats
import nl.edejong.eve.http.HttpFlows

import scala.collection.immutable
import scala.concurrent.Future

trait EveAPIs extends Domain with DomainFormats {

  trait MarketAPI extends Markets with MarketFormats {
    def orders(regionId: RegionId)(implicit materializer: ActorMaterializer): Future[immutable.Seq[Order]]

    def ordersFlow: Flow[RegionId, (Order, RegionId), NotUsed]

    def historicOrders(regionId: RegionId, typeId: TypeId)(implicit materializer: ActorMaterializer): Future[immutable.Seq[HistoricOrder]]

    def historicOrdersFlow: Flow[(RegionId, TypeId), (HistoricOrder, (RegionId, TypeId)), NotUsed]

    def types(regionId: Long)(implicit materializer: ActorMaterializer): Future[Seq[TypeId]]
  }

  trait UniverseAPI extends Universe with UniverseFormats with HttpFlows {
    def regionIds: Future[Seq[RegionId]]

    def region(id: RegionId): Future[Region]

    def typeIds: Future[Seq[TypeId]]

    def eveType(id: RegionId): Future[EveType]
  }

}


