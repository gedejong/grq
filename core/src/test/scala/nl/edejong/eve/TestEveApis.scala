package nl.edejong.eve

import akka.NotUsed
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import nl.edejong.eve.api.EveAPIs

import scala.collection.immutable.Seq
import scala.concurrent.Future

trait TestEveApis extends EveAPIs with TestEnvironments {

  trait TestUniverseAPI extends UniverseAPI with TestUniverseEnvironment {
    override def regionIds: Future[Seq[RegionId]] = Future.successful(testRegions.map(_.regionId))

    override def region(id: RegionId): Future[Region] =
      testRegions.find(_.regionId == id) match {
        case Some(region) ⇒ Future.successful(region)
        case None ⇒ Future.failed(new IllegalStateException("Region not found"))
      }

    override def typeIds: Future[Seq[TypeId]] =
      Future.successful(testTypes.map(_.typeId))

    override def eveType(id: RegionId): Future[EveType] =
      testTypes.find(_.typeId == id) match {
        case Some(eveType) ⇒ Future.successful(eveType)
        case None ⇒ Future.failed(new IllegalStateException("Type not found"))
      }
  }

  trait TestMarketAPI extends MarketAPI with TestMarketEnvironment {
    override def orders(regionId: RegionId)(implicit materializer: ActorMaterializer): Future[Seq[Order]] =
      testOrders.find { case (region, _) ⇒ region.regionId == regionId } match {
        case Some((_, orderSeq)) ⇒ Future.successful(orderSeq)
        case None ⇒ Future.successful(Seq())
      }

    override def ordersFlow: Flow[RegionId, (Order, RegionId), NotUsed] =
      Flow.fromFunction[RegionId, Seq[(Order, RegionId)]] { regionId ⇒
        testOrders.find(_._1.regionId == regionId).map(_._2.map((_, regionId))).getOrElse(Seq[(Order, RegionId)]())
      }.expand(o ⇒ o.iterator)

    override def historicOrders(regionId: RegionId, typeId: TypeId)(implicit materializer: ActorMaterializer): Future[Seq[HistoricOrder]] =
      testHistoricOrders
        .find { case ((region, eveType), _) ⇒ region.regionId == regionId && eveType.typeId == typeId } match {
        case Some((_, historicOrders)) ⇒ Future.successful(historicOrders)
        case None ⇒ Future.successful(Seq())
      }

    override def historicOrdersFlow: Flow[(RegionId, TypeId), (HistoricOrder, (RegionId, TypeId)), NotUsed] =
      Flow.fromFunction[(RegionId, TypeId), Seq[(HistoricOrder, (RegionId, TypeId))]] {
        case (regionId, typeId) ⇒
          testHistoricOrders.find { case ((region, eveType), _) ⇒ region.regionId == regionId && eveType.typeId == typeId }
            .map(_._2.map((_, (regionId, typeId)))).getOrElse(Seq())
      }.expand(o ⇒ o.iterator)

    override def types(regionId: TypeId)(implicit materializer: ActorMaterializer): Future[Seq[TypeId]] =
      Future.successful(testTypes.map(_.typeId))
  }

}

