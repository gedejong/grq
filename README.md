# Eve Online Market scraper and notifier

Demo application using akka-streams and Apache Kafka to 
scrape the Eve Online market orders for specific orders which
are exceptionally cheap.

## Overview

The project consists of three modules:

- `core`: containing EVE online domain models, `play-json ` formats, configuration
tooling, Kafka sources and sinks and test environments.

- `scraper`, uses `core' to periodically (every 5 minutes) load all available orders
and place them on the Kafka bus. No filtering in the Orders is done at this moment,
so duplicate orders might appear.

- `notifier`, sources the Kafka bus for orders, creates a simple Actor hierarchy, with
one actor handling a region/type combination. When a new actor is created, it
will periodically check the historic prices and compute a weighted average price.
When an (sell) order is found with an extraordinarily low price, the notifier will
write a message to standard out.

## How to build and run

### Local Kafka / Zookeeper

1. Start a Zookeeper / Kafka instance 
2. Deploy the docker instances: `sbt docker:publishLocal`
3. Run the notifier: `docker run -e 'EVE_KAFKA_BOOTSTRAP_SERVERS=<hostname:port> 845799947042.dkr.ecr.eu-west-1.amazonaws.com/eve-notifier:0.1-SNAPSHOT`
3. Run the scraper: `docker run -e 'EVE_KAFKA_BOOTSTRAP_SERVERS=<hostname:port> 845799947042.dkr.ecr.eu-west-1.amazonaws.com/eve-scraper:0.1-SNAPSHOT`

### Docker only setup

```shell 
# Build and deploy the docker instances (locally)
sbt docker:publishLocal

# Create a docker network
docker network create kafka-net

# Run zookeeper
docker run -d --name zookeeper --network kafka-net zookeeper:3.4

# Run Kafka
docker run -d --name kafka --network kafka-net --env ZOOKEEPER_IP=zookeeper ches/kafka

# Run the notifier:
docker run -e 'EVE_KAFKA_BOOTSTRAP_SERVERS=kafka:9092' --rm --interactive --network kafka-net 845799947042.dkr.ecr.eu-west-1.amazonaws.com/eve-notifier:0.1-SNAPSHOT

# Run the scraper:
docker run -e 'EVE_KAFKA_BOOTSTRAP_SERVERS=kafka:9092' --rm --interactive --network kafka-net 845799947042.dkr.ecr.eu-west-1.amazonaws.com/eve-scraper:0.1-SNAPSHOT
```

```
