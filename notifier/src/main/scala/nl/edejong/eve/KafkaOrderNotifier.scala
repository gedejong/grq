package nl.edejong.eve

import akka.Done
import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import com.typesafe.config.ConfigFactory
import nl.edejong.eve.actors.EveActors
import nl.edejong.eve.api.{EveHttpAPIs, HoconEveHttpAPIConfiguration}
import nl.edejong.eve.kafka.{EveKafkaStreams, HoconEveKafkaConfiguration}

import scala.concurrent.Await


object KafkaOrderNotifier
  extends App
    with EveKafkaStreams
    with EveActors
    with EveHttpAPIs
    with HoconEveHttpAPIConfiguration
    with HoconEveKafkaConfiguration
    with DiagnosticActors {

  override lazy val config = ConfigFactory.load()

  object api
    extends OrderKafkaSource
      with MarketActors
      with HttpUniverseAPI
      with HttpMarketAPI
      with DiagnosticMarketActors
      with HoconOrderKafkaSourceConfiguration

  import api._

  override lazy val actorSystem = ActorSystem()

  val stdOutActorRef = actorSystem.actorOf(ToLogActor.props())
  val globalOrdersActor = actorSystem.actorOf(GlobalOrdersActor.props(stdOutActorRef))
  val control = orderKafkaSource.to(Sink.actorRef(globalOrdersActor, Done)).run()
  sys.addShutdownHook { () ⇒
    import scala.concurrent.duration._
    Await.result(for {
      shutdownFuture ← control.shutdown()
      systemTermination ← actorSystem.terminate()
    } yield (shutdownFuture, systemTermination), 10.seconds)
  }
}
