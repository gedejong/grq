package nl.edejong.eve.kafka

trait EveKafkaConfiguration {
  protected def bootstrapServers: String

  trait OrderKafkaStreamConfiguration {
    protected def topicId: String
  }

  trait OrderKafkaSourceConfiguration extends OrderKafkaStreamConfiguration {
    protected def groupId: String
  }

  trait OrderKafkaSinkConfiguration extends OrderKafkaStreamConfiguration

}
