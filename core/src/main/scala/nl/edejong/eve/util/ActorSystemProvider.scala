package nl.edejong.eve.util

import akka.actor.ActorSystem

trait ActorSystemProvider {
  def actorSystem: ActorSystem
}
