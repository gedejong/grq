package nl.edejong.eve

import java.time.{LocalDate, OffsetDateTime}

import nl.edejong.eve.api.domain.Domain

import scala.collection.immutable.Seq

trait TestEnvironments extends Domain {

  trait TestUniverseEnvironment extends Universe with Markets {
    def testRegions: Seq[Region]

    def testTypes: Seq[EveType]
  }

  trait TestMarketEnvironment extends TestUniverseEnvironment with Markets {
    def testOrders: Map[Region, Seq[Order]]

    def testHistoricOrders: Map[(Region, EveType), Seq[HistoricOrder]]
  }

  trait TestUniverseEnvironmentHelpers extends Universe with Markets {
    def createTestEveType(id: TypeId) =
      EveType(typeId = id, name = s"Type $id", description = s"Type $id", published = true,
        groupId = 1L, radius = 1L, volume = 1L, packagedVolume = 1L,
        capacity = 1L, portionSize = 1L, mass = 1L, graphicId = 1L)
  }

  trait TestMarketEnvironmentHelpers extends Markets with Universe {
    def createTestOrder(orderId: OrderId, eveType: EveType, price: Double, issued: OffsetDateTime = OffsetDateTime.now()): Order =
      Order(orderId = orderId, typeId = eveType.typeId, locationId = 1L, volumeTotal = 1L, volumeRemain = 1L,
        minVolume = 1L, price = price, isBuyOrder = false, duration = 1000, issued = OffsetDateTime.now(), range = "region")

    def createTestHistoricOrder(day: Int, volume: Long, average: Double): HistoricOrder =
      HistoricOrder(
        date = LocalDate.now.minusWeeks(1).plusDays(day),
        orderCount = 10,
        volume = volume,
        highest = average * 1.2,
        average = average,
        lowest = average * 0.8)

    def createTestHistoricOrdersConstantWeek(volume: Long, average: Double): Seq[HistoricOrder] =
      for (day ← 0 to 7) yield createTestHistoricOrder(day, volume, average)
  }

  trait StandardUniverseEnvironment extends TestUniverseEnvironment with TestUniverseEnvironmentHelpers {

    val region1 = Region(1, "Region 1", Seq(1, 2, 3), "Region 1")
    val region2 = Region(1, "Region 2", Seq(4, 2, 3), "Region 2")
    val region3 = Region(3, "Region 3", Seq(1, 2, 3), "Region 3")
    override val testRegions = Seq(region1, region2, region3)
    val type1: EveType = createTestEveType(1)
    val type2: EveType = createTestEveType(2)
    override val testTypes = Seq(type1, type2)
  }

  trait StandardMarketEnvironment extends StandardUniverseEnvironment with TestMarketEnvironment with TestMarketEnvironmentHelpers {
    override val testOrders: Map[Region, Seq[Order]] = Map(
      region1 → Seq(createTestOrder(1L, type1, 123D), createTestOrder(2L, type2, 321D)),
      region2 → Seq(),
      region3 → Seq(createTestOrder(3L, type1, 432D)))

    override val testHistoricOrders: Map[(Region, EveType), Seq[HistoricOrder]] = Map(
      (region1, type1) → createTestHistoricOrdersConstantWeek(1000, 200D),
      (region2, type2) → createTestHistoricOrdersConstantWeek(2000, 300D),
      (region3, type1) → createTestHistoricOrdersConstantWeek(3000, 400D),
      (region3, type2) → createTestHistoricOrdersConstantWeek(300, 40D))
  }

}
