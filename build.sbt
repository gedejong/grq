name := "get-rich-quick-consumer"
version := "0.1"
scalaVersion := "2.12.3"

val akkaVersion = "2.5.6"
val akkaStreamKafkaVersion = "0.17"
val akkaHttpVersion = "10.0.10"
val circeVersion = "0.8.0"
val playJsonVersion = "2.6.6"

/** Docker and deployment */
lazy val core = (project in file("core"))
  .settings(
    name := "eve-api-core",
    libraryDependencies ++= Seq(
      // Akka related
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream-kafka" % akkaStreamKafkaVersion,
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,

      // Akka HTTP
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test",

      // Play-json
      "com.typesafe.play" %% "play-json" % playJsonVersion,

      // Play json -- Akka HTTP bridge
      "de.heikoseeberger" %% "akka-http-play-json" % "1.18.0",

      // Logging
      "ch.qos.logback" % "logback-classic" % "1.0.9",

      // Scalatest
      "org.scalatest" %% "scalatest" % "3.0.4" % "test"
    )
  )

val dockerSettings = Seq(
  maintainer := "Edwin de Jong <edejong@fastmail.fm>",
  dockerRepository := Some("845799947042.dkr.ecr.eu-west-1.amazonaws.com")
)

lazy val scraper = (project in file("scraper"))
  .enablePlugins(JavaAppPackaging)
  .settings(dockerSettings)
  .settings(
    name := "scraper",
    packageName := "eve-scraper",
    packageSummary := "A scraper for Eve online market data",
    packageDescription := "Scrapes Eve Online market data and puts it on a specifc Kafka bus"
  )
  .dependsOn(core % "test->test;compile->compile")

lazy val notifier = (project in file("notifier"))
  .enablePlugins(JavaAppPackaging)
  .settings(dockerSettings)
  .settings(
    name := "notifier",
    packageName := "eve-notifier",
    packageSummary := "A Kafka listener which notifies when orders are priced too low",
    packageDescription := "Listens to a Kafka bus and notifies when the price of an order is too low",
  )
  .dependsOn(core % "test->test;compile->compile")

lazy val root = (project in file(".")).aggregate(notifier, scraper).settings(
  packagedArtifacts := Map.empty,
  publish := {}
)
