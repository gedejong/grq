package nl.edejong.eve

import akka.actor.ActorSystem
import akka.stream.scaladsl.Keep
import com.typesafe.config.ConfigFactory
import nl.edejong.eve.api.{EveHttpAPIs, HoconEveHttpAPIConfiguration}
import nl.edejong.eve.kafka.{EveKafkaStreams, HoconEveKafkaConfiguration}

import scala.concurrent.Await

object ToKafkaOrderScraper
  extends Scrapers
    with EveKafkaStreams
    with EveHttpAPIs
    with HoconEveHttpAPIConfiguration
    with HoconEveKafkaConfiguration {

  override lazy val actorSystem: ActorSystem = ActorSystem()
  override lazy val config = ConfigFactory.load()

  object api
    extends EveOrderScraper
      with OrderKafkaSink
      with HttpMarketAPI
      with HttpUniverseAPI
      with HoconOrderKafkaSinkConfiguration

  val (killSwitch, done) = api.ordersScraper.toMat(api.orderKafkaSink)(Keep.both).run

  sys.addShutdownHook { () ⇒
    import scala.concurrent.duration._
    killSwitch.shutdown()
    Await.result(done, 20.seconds)
    Await.result(actorSystem.terminate(), 10.seconds)
  }
}
