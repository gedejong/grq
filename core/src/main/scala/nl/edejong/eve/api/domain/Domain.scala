package nl.edejong.eve.api.domain

import java.time.{LocalDate, OffsetDateTime}

import scala.collection.immutable.Seq

trait Domain {
  type RegionId = Long
  type ConstellationId = Long
  type LocationId = Long
  type TypeId = Long
  type GroupId = Long
  type GraphicId = Long
  type OrderId = Long

  trait Markets {

    case class Order(
                      orderId: OrderId,
                      typeId: TypeId,
                      locationId: LocationId,
                      volumeTotal: Long,
                      volumeRemain: Long,
                      minVolume: Long,
                      price: Double,
                      isBuyOrder: Boolean,
                      duration: Long,
                      issued: OffsetDateTime,
                      range: String)

    case class HistoricOrder(
                              date: LocalDate,
                              orderCount: Long,
                              volume: Long,
                              highest: Double,
                              average: Double,
                              lowest: Double)
  }

  trait Universe {

    case class Region(regionId: RegionId, name: String, constellations: Seq[ConstellationId], description: String)

    case class EveType(
                        typeId: TypeId,
                        name: String,
                        description: String,
                        published: Boolean,
                        groupId: GroupId,
                        radius: Long,
                        volume: Long,
                        packagedVolume: Long,
                        capacity: Long,
                        portionSize: Long,
                        mass: Long,
                        graphicId: GraphicId)

  }

}
