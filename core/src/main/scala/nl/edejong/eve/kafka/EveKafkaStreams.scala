package nl.edejong.eve.kafka

import akka.Done
import akka.kafka.scaladsl.{Consumer, Producer}
import akka.kafka.{ConsumerSettings, ProducerSettings, Subscriptions}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import nl.edejong.eve.api.domain.Domain
import nl.edejong.eve.api.formats.DomainFormats
import nl.edejong.eve.util.ActorSystemProvider
import org.apache.kafka.clients.producer.ProducerRecord

import scala.concurrent.Future


trait EveKafkaStreams extends ActorSystemProvider with Domain with PlayJsonKafkaSupport
  with DomainFormats with ScalaKafkaSupport with EveKafkaConfiguration {

  trait OrderKafkaSource extends Markets with MarketFormats with OrderKafkaSourceConfiguration {
    private[this] val consumerSettings = ConsumerSettings[RegionId, Order](
      system = actorSystem,
      keyDeserializer = new LongDeserializer,
      valueDeserializer = new PlayJsonDeserializer[Order])
      .withGroupId(groupId)
      .withBootstrapServers(bootstrapServers)

    val orderKafkaSource: Source[(Order, RegionId), Consumer.Control] =
      Consumer.plainSource(consumerSettings, Subscriptions.topics(topicId))
        .map(consumerRecord ⇒ (consumerRecord.value(), consumerRecord.key()))
  }

  trait OrderKafkaSink extends Markets with MarketFormats with OrderKafkaSinkConfiguration {

    private[this] val producerSettings = ProducerSettings[RegionId, Order](
      system = actorSystem,
      keySerializer = new LongSerializer,
      valueSerializer = new PlayJsonSerializer[Order]).withBootstrapServers(bootstrapServers)

    val orderKafkaSink: Sink[(Order, RegionId), Future[Done]] =
      Flow[(Order, RegionId)]
        .map { case (order, regionId) ⇒ new ProducerRecord[Long, Order](topicId, regionId, order) }
        .toMat(Producer.plainSink(producerSettings))(Keep.right)
  }

}
