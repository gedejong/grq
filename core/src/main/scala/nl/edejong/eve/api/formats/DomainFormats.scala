package nl.edejong.eve.api.formats

import java.time.{LocalDate, OffsetDateTime}

import nl.edejong.eve.api.domain.Domain
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json.Writes._
import play.api.libs.json._

import scala.collection.immutable.Seq

trait DomainFormats extends Domain {

  trait MarketFormats extends Markets {
    implicit val orderFormat: OFormat[Order] = (
      (__ \ "order_id").format[Long] and
        (__ \ "type_id").format[Long] and
        (__ \ "location_id").format[Long] and
        (__ \ "volume_total").format[Long] and
        (__ \ "volume_remain").format[Long] and
        (__ \ "min_volume").format[Long] and
        (__ \ "price").format[Double] and
        (__ \ "is_buy_order").format[Boolean] and
        (__ \ "duration").format[Long] and
        (__ \ "issued").format[OffsetDateTime] and
        (__ \ "range").format[String]
      ) (Order, unlift(Order.unapply))

    implicit val historicOrderFormat: OFormat[HistoricOrder] = (
      (__ \ "date").format[LocalDate] and
        (__ \ "order_count").format[Long] and
        (__ \ "volume").format[Long] and
        (__ \ "highest").format[Double] and
        (__ \ "average").format[Double] and
        (__ \ "lowest").format[Double]
      ) (HistoricOrder, unlift(HistoricOrder.unapply))
  }

  trait UniverseFormats extends Universe {
    implicit val regionFormat: Format[Region] = (
      (__ \ "region_id").format[RegionId] and
        (__ \ "name").format[String] and
        (__ \ "constellations").format[Seq[ConstellationId]] and
        (__ \ "description").format[String]
      ) (Region, unlift(Region.unapply))

    implicit val eveTypeFormat: Format[EveType] = (
      (__ \ "type_id").format[TypeId] and
        (__ \ "name").format[String] and
        (__ \ "description").format[String] and
        (__ \ "published").format[Boolean] and
        (__ \ "group_id").format[GroupId] and
        (__ \ "radius").format[Long] and
        (__ \ "volume").format[Long] and
        (__ \ "packaged_volume").format[Long] and
        (__ \ "capacity").format[Long] and
        (__ \ "portion_size").format[Long] and
        (__ \ "mass").format[Long] and
        (__ \ "graphic_id").format[GraphicId]
      ) (EveType, unlift(EveType.unapply))
  }

}
