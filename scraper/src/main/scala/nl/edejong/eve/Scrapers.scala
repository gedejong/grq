package nl.edejong.eve

import akka.event.Logging
import akka.stream.scaladsl.{Keep, RestartSource, Source}
import akka.stream.{Attributes, KillSwitches, ThrottleMode, UniqueKillSwitch}
import nl.edejong.eve.api.EveAPIs
import nl.edejong.eve.api.domain.Domain

import scala.concurrent.duration.FiniteDuration

trait Scrapers extends App with Domain with EveAPIs {

  trait ScraperConfig {
    def tickFrequency: FiniteDuration

    def regionPollThrottle: FiniteDuration
  }

  trait EveOrderScraper extends MarketAPI with UniverseAPI {

    import scala.concurrent.duration._

    val ordersScraper: Source[(Order, RegionId), UniqueKillSwitch] =
      RestartSource.withBackoff(minBackoff = 30.seconds, maxBackoff = 30.minutes, randomFactor = 0.02) { () ⇒
        Source.tick[Any](0.seconds, 5.minutes, AnyRef)
          .withAttributes(Attributes.logLevels(onElement = Logging.WarningLevel))
          .mapAsync(1) { _ => regionIds }
          .expand(_.iterator)
          .throttle(elements = 10, per = 2.second, maximumBurst = 2, mode = ThrottleMode.shaping)
          .log("region_id")
          .via(ordersFlow)
      }.viaMat(KillSwitches.single)(Keep.right)
  }

}
