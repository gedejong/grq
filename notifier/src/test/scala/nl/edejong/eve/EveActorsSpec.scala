package nl.edejong.eve

import java.time.{OffsetDateTime, ZoneOffset}

import akka.actor._
import akka.testkit.{ImplicitSender, TestKit}
import nl.edejong.eve.actors.EveActors
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class EveActorsSpec() extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with TestEnvironments with TestEveApis with EveActors {

  object testMarketActorEnvironment
    extends StandardUniverseEnvironment with StandardMarketEnvironment
      with TestUniverseAPI with TestMarketAPI with MarketActors

  private val currently = OffsetDateTime.of(2017, 10, 29, 10, 59, 0, 0, ZoneOffset.UTC)

  import testMarketActorEnvironment._
  import MarketActorBehaviour._

  import scala.concurrent.duration._

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "An GlobalOrdersActor" must {
    "reply with a price notification when an order with a low price is sent" in {
      val globalOrdersActorRef = system.actorOf(GlobalOrdersActor.props(self))
      val testOrder = createTestOrder(1, type1, 0.1D, issued = currently)
      globalOrdersActorRef ! (testOrder, region1.regionId)
      expectMsg(PriceNotification(regionId = region1.regionId, typeId = type1.typeId, order = testOrder, historicPrice = 0.2D))
    }

    "reply with no price notification when a normal order is sent" in {
      val globalOrdersActorRef = system.actorOf(GlobalOrdersActor.props(self))
      val testOrder = createTestOrder(1, type1, 0.3D, issued = currently)
      globalOrdersActorRef ! (testOrder, region1.regionId)
      expectNoMsg(1.second)
    }

    "reply only once when the same order is seen twice" in {
      val globalOrdersActorRef = system.actorOf(GlobalOrdersActor.props(self))
      val testOrder1 = createTestOrder(1L, type1, 0.1D, issued = currently)
      val testOrder2 = createTestOrder(1L, type1, 0.1D, issued = currently)
      globalOrdersActorRef ! (testOrder1, region1.regionId)
      globalOrdersActorRef ! (testOrder2, region1.regionId)
      expectMsgType[PriceNotification].order.orderId should be(1L)
      expectNoMsg(1.second)
    }
  }

}
